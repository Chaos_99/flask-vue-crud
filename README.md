# A coding exercise with Python flask and Vue.js

### Based on

Check out the [post on testdriven.io](https://testdriven.io/developing-a-single-page-app-with-flask-and-vuejs).

## Execution and development

### classic approach
1. Fork/Clone

1. Run the server-side Flask app in one terminal window:

    ```sh
    $ cd server
    $ python3.9 -m venv env
    $ source env/bin/activate
    (env)$ pip install -r requirements.txt
    (env)$ python app.py
    ```

    Navigate to [http://localhost:5000](http://localhost:5000)

1. Run the client-side Vue app in a different terminal window:

    ```sh
    $ cd client
    $ npm install
    $ npm run serve
    ```

    Navigate to [http://localhost:8080](http://localhost:8080)

### gitpod

  1. clone this repo
  2. open this in gitpod be pre-pending the gitlab URL with gitpod.io/#

  The .gitpod.dockerfile file will set up the gitpod environment to prebuild the server and client and initialize the demo.

  You may need to overwrite the hard-coded URLs in the client to match the gitpod unique container urls.

## The project

This is *ANOIA 2.0*, a material database for our local maker-/hackspace.
It's aimed at providing an interface on either a touch-screen or a dumb screen with a barcode scanner as the only input device.

### Status

  - backend has no database yet, just in-memory storage
  - flask exposes a simple CRUD API
  - using flask dev server, needs to be rplaced with gunicorn and nginx for prod
  - vue.js client only has a debugging frontend

## Disclaimer

I have no prior experience with web-dev. Under no circumstance use this code as is on the internet.