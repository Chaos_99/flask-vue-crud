FROM gitpod/workspace-full

RUN sudo apt-get update  && sudo apt-get upgrade -y
RUN sudo apt-get install -y npm
RUN sudo apt-get install -y nodejs
RUN sudo apt-get install -y mariadb-server
RUN sudo npm install vue eslint eslint-plugin-import eslint-plugin-node eslint-plugin-promise eslint-plugin-standard eslint-plugin-vue
RUN sudo npm install @vue/eslint-config-standard babel-eslint
#RUN sudo npm audit fix


