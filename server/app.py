"""Main server flask application"""

import uuid
import logging

from flask import Flask, jsonify, request
from flask_cors import CORS

INVENTORY = [
    {
        'id': uuid.uuid4().hex,
        'name': 'M4x60 stainless',
        'amount': 30,
        'storage': {'room': 'MS_Social',
                    'rack': 'Sortierregal',
                    'position': 'R3 S8'},
        'price': 0.15,
        'extras': None
    },
    {
        'id': uuid.uuid4().hex,
        'name': 'Laminierfolien',
        'amount': 100,
        'storage': {'room': 'MS_Laser',
                    'rack': 'Schwerlast',
                    'position': 'R1 F3'},
        'price': 2.50 / 100,
        'extras': None
    },
    {
        'id': uuid.uuid4().hex,
        'name': 'Schwingschleifer',
        'amount': 1,
        'storage': {'room': 'MS_Holz',
                    'rack': 'Werbank',
                    'position': 'rechts unten'},
        'price': 40.00,
        'extras': [{'name': 'Status', 'type': 'string', 'value': 'einsatzbereit'}]
    },
]

# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

# sanity check route
@app.route('/ping', methods=['GET'])
def ping_pong():
    """ Debugging function for the 'ping'endpoint"""
    return jsonify('pong!')


def remove_item(item_id):
    """ Remove an item from the db"""
    for item in INVENTORY:
        if item['id'] == item_id:
            INVENTORY.remove(item)
            return True
    return False


@app.route('/items', methods=['GET', 'POST'])
def all_items():
    """ Retrun a json of all db items"""
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        temp_item = {
            'id': uuid.uuid4().hex,
            'name': post_data.get('name'),
            'amount': post_data.get('amount'),
            'storage': {'room': post_data.get('room'),
                        'rack': post_data.get('rack'),
                        'position': post_data.get('position')},
            'price': post_data.get('price')
        }
        # extras = post_data.get('extras')
        # if extras is not None:
        #     temp_item['extras'] = [{'name': 'Status', 'type': 'string', 'value': 'einsatzbereit'}]
        INVENTORY.append(temp_item)
        response_object['message'] = 'Item added!'
    else:
        response_object['items'] = INVENTORY
    return jsonify(response_object)


@app.route('/items/<item_id>', methods=['PUT', 'DELETE'])
def single_item(item_id):
    """ Single item acces for editing or deleting"""
    response_object = {'status': 'success'}
    if request.method == 'PUT':
        post_data = request.get_json()
        if DEBUG:
            logging.debug(print(post_data))
        remove_item(item_id)
        temp_item = {
            'id': uuid.uuid4().hex,
            'name': post_data.get('name'),
            'amount': post_data.get('amount'),
            'storage': {'room': post_data.get('storage').get('room'),
                        'rack': post_data.get('storage').get('rack'),
                        'position': post_data.get('storage').get('position')},
            'price': post_data.get('price')
        }

        # extras = post_data.get('extras')
        # if extras is not None:
        #     temp_item['extras'] = [{'name': 'Status', 'type': 'string', 'value': 'einsatzbereit'}]
        INVENTORY.append(temp_item)
        response_object['message'] = 'Item updated!'
    if request.method == 'DELETE':
        remove_item(item_id)
        response_object['message'] = 'Item removed!'
    return jsonify(response_object)

@app.after_request
def add_headers(response):
    """ Add site headers for webserver"""
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = "Content-Type, Access-Control-Allow-Headers"\
                                                       ", Authorization, X-Requested-With"
    response.headers['Access-Control-Allow-Methods'] = "POST, GET, PUT, DELETE, OPTIONS"
    return response

if __name__ == '__main__':
    app.run()
