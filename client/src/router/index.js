import Vue from 'vue';
import Router from 'vue-router';
import ItemsView from '../components/ItemsView.vue';
import PingDebug from '../components/PingDebug.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Items',
      component: ItemsView,
    },
    {
      path: '/ping',
      name: 'Ping',
      component: PingDebug,
    },
  ],
});
